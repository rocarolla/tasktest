'use strict';
angular.module('taskApp', []);


angular.module('taskApp').directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){

                $(element).tooltip('show');
            }, function(){

               $(element).tooltip('hide');
            });
        }
    };
});