'use strict';
angular.module('taskApp')
    .controller('TaskController', ['$scope', '$filter', 'TaskStorage', TaskController]);

function TaskController($scope, $filter, TaskStorage) {

    $scope.TaskStorage = TaskStorage;
    $scope.TaskStorage.LoadFromStorage();
    $scope.taskDefaultTime = 10;
    $scope.TaskModel = {};

    $scope.InitTaskModel = function () {
        var d = new Date();
        d.setMinutes(d.getMinutes() + $scope.taskDefaultTime);

        $scope.curDate = $filter('date')(d, "dd/MM/yyyy");
        $scope.curTime = $filter('date')(d, "HH:mm");

        $scope.TaskModel = {
            title: '',
            description: '',
            endDateMs: 0,
            difference: 0,
            differenceAsTimeString: ''

        };
    };

    $scope.InitTaskModel();


    $scope.CreateTask = function () {
        $scope.calculateTaskEndDate();
        $scope.TaskStorage.AddTask($scope.TaskModel);
        $scope.InitTaskModel();


    };

    $scope.calculateTaskEndDate = function () {

        var date = $scope.curDate.split('/');
        var time = $scope.curTime.split(':');
        $scope.TaskModel.endDateMs = new Date(date[2], date[1] - 1, date[0], time[0], time[1], new Date().getSeconds(), 0);

    };

    $scope.addExtraTime = function (index) {

        $scope.TaskStorage.AddExtraTime(index, $scope.taskDefaultTime);

    };

    $scope.removeTask = function (index) {

        $scope.TaskStorage.RemoveTask(index);
    };


}