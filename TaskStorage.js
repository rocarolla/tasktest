'use strict';
angular.module('taskApp')
    .factory('TaskStorage', ['$timeout', function ($timeout) {

        var tasks = [];
        var destroyTime = -120000;
        var saveInStorage = function () {

            localStorage.setItem("tasks", JSON.stringify(tasks));
        };

        var loadFromStorage = function () {

            var data = localStorage.getItem("tasks");

            tasks = JSON.parse(data) || [];

            angular.forEach(tasks, function (task) {

                task.endDateMs = new Date(task.endDateMs);
            });

            updater();

        };


        var getTasks = function () {

            return tasks;
        };

        var addTask = function (task) {

            tasks.push(task);
            saveInStorage();
        };

        var removeTask = function (index) {

            tasks.splice(index, 1);
            saveInStorage();
        };

        var addExtraTime = function (index, time) {

            tasks[index].endDateMs.setMinutes(tasks[index].endDateMs.getMinutes() + time)
        };

        var calculateDifferenceAsTimeString = function () {

            angular.forEach(tasks, function (task, index) {

                var diff = new Date(task.endDateMs) - new Date();
                var seconds = parseInt(diff / 1000 % 60);
                if (seconds < 0)seconds = 0;
                var minutes = parseInt(diff / 1000 / 60 % 60);
                if (minutes < 0)minutes = 0;
                task.differenceAsTimeString = parseInt(diff / 1000 / 3600) + ":" + minutes + ":" + seconds;
                task.difference = diff;

                if (task.difference < destroyTime) removeTask(index);
            });


        };


        var updater = function upd() {
            $timeout(function () {
                calculateDifferenceAsTimeString();
                upd();
            }, 1000);
        };


        return {

            GetTasks: getTasks,
            AddTask: addTask,
            RemoveTask: removeTask,
            LoadFromStorage: loadFromStorage,
            AddExtraTime: addExtraTime

        };
    }]);
